<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Category extends REST_Controller {

    	function __construct()
    	{
	        // Construct the parent class
	        parent::__construct();
	        
	        error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
	        
	        //$this->load->model('Ticket_Model');
    	}

    	public function index_get()
	  {
	 	//$ticket_response_category= $this->Ticket_Model->get_ticket_response_category();			
	//	$this->set_response($ticket_response_category, REST_Controller::HTTP_OK);	
	echo "Hello";
	  }
	  
	  
}
