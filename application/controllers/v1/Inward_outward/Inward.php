<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/**
 * @noinspection PhpIncludeInspection
 */
// require APPPATH . 'libraries/REST_Controller.php';
require (APPPATH . '/libraries/REST_Controller.php');

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package CodeIgniter
 * @subpackage Rest Server
 * @category Controller
 * @author Phil Sturgeon, Chris Kacerguis
 * @license MIT
 * @link https://github.com/chriskacerguis/codeigniter-restserver
 */
class Inward extends REST_Controller {
	function __construct() {
		// Construct the parent class
		parent::__construct ();
		
		error_reporting ( E_ALL ^ (E_NOTICE | E_WARNING) );
		//$this->load->helper ( 'authorization_helper' );
		
    $this->load->database ();
	$this->load->model ( 'Inward_Model' );
	}
	

	
	/**
	 * ********** Start Of Function To Save Assets Details *************
	 */
	public function index_get() {
	  //  echo 'hi';
	/*	$assets_name = $_POST ['assets_name'];
		$assets_type_id = $_POST ['assets_type_id'];
		$assets_date = $_POST ['assets_date'];
		$assets_amount = $_POST ['assets_amount'];
		$assets_description = $_POST ['assets_description'];
	
		
		
		$assets_details = $this->Assets_Model->store_assets ( $assets_name, $assets_type_id, $assets_date, $assets_amount, $assets_description);
		
		print_r ( json_encode ( $assets_details ) );
		*/
		$assets_details = $this->Inward_Model->save ();
		print_r ( json_encode ( $assets_details ) );
	}
	/**
	 * ********** Start Of Function To Save Assets Details *************
	 */
	

	
}