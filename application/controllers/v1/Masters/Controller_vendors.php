<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/**
 * @noinspection PhpIncludeInspection
 */
// require APPPATH . 'libraries/REST_Controller.php';
require (APPPATH . '/libraries/REST_Controller.php');

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package CodeIgniter
 * @subpackage Rest Server
 * @category Controller
 * @author Phil Sturgeon, Chris Kacerguis
 * @license MIT
 * @link https://github.com/chriskacerguis/codeigniter-restserver
 */
class Controller_vendors extends REST_Controller {
	function __construct() {
		// Construct the parent class
		parent::__construct ();
		
		error_reporting ( E_ALL ^ (E_NOTICE | E_WARNING) );
		//$this->load->helper ( 'authorization_helper' );
		
    $this->load->database ();
	$this->load->model ( 'Vendor_Model' );
	}
	

	
	/**
	 * ********** Start Of Function To Save Assets Details *************
	 */
	public function index_post() {
		$name='Coreco';

		//echo $name;
		//echo "Hi";
	
		$supplier_data = array (
			'supplier_company_name' => $name,
		 'supplier_contact_person_firstName' => 'Nagraj',
		 'supplier_contact_person_lastName' => 'Kirmate',
			
			'supplier_address' => 'Pune',
			'supplier_city' => 'Pune' ,
			'supplier_pin' => 411107,
			'supplier_phone' => 1234567890,
			'supplier_status' => 'Active',
			'supplier_role' => 1,
			'company_id' => 1,
			'non_supplier_flag' => 0 
			);	
		
			print_r($supplier_data);
	}
	/**
	 * ********** Start Of Function To Save Assets Details *************
	 */
    public function master_vendor_new_post()
    {
        // $this->some_model->update_user( ... );


       

        // $message = [
            
        //     'vname' => $this->post('vname'),
        //     'vadd1' => $this->post('vadd1'),
        //     'vadd2' => $this->post('vadd2'),
        //     'vadd3' => $this->post('vadd3'),
        //     'vadd4' => $this->post('vadd4'),
        //     'vcno' => $this->post('vcno'),
        //     'vacno' => $this->post('vacno'),
        //     'vpin' => $this->post('vpin'),
        //     'vemail' => $this->post('vemail'),
        //     'vcp' => $this->post('vcp'),
            
		// ];



		// $message = array(
		// 	'vname => 'dsfs',
        //     $vadd1 => "dsfs",
		// 	$vadd2 => "dsfs",
        //     $vadd3=>"dsfs",
        //     $vadd4=>  "dsfs",
        //     $vcno=> "dsfs",
        //     $vacno=> "dsfs",
        //     $vpin=> "dsfs",
        //     $vemail=> "dsfs",
        //     $vcp=>"dsfs"
		// );

		$name='Coreco';

		echo $name;
		echo "Hi";
	
		$supplier_data = array (
			'supplier_company_name' => $name,
		 'supplier_contact_person_firstName' => 'Nagraj',
		 'supplier_contact_person_lastName' => 'Kirmate',
			
			'supplier_address' => 'Pune',
			'supplier_city' => 'Pune' ,
			'supplier_pin' => 411107,
			'supplier_phone' => 1234567890,
			'supplier_status' => 'Active',
			'supplier_role' => 1,
			'company_id' => 1,
			'non_supplier_flag' => 0 
			);	
		
			print_r($supplier_data);


        //$vendor_details = $this->Vendor_Model->save($message);
		//print_r ( json_encode ( $vendor_details ) );

        //$this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }

	
}