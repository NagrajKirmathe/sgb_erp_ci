<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class Audit_log_Model extends CI_Model {
	public function __construct() {
		parent::__construct ();
		$this->load->database ();
		$this->load->library ( 'session' );
	}
	public function login_audit_log($id, $query) {
		date_default_timezone_set ( 'Asia/Kolkata' );
		$company_id = $this->session->userdata ( 'company_id' );
		$user_id = $this->session->userdata ( 'user_id' );
		$current_date = date ( "Y-m-d" );
		$current_time = date ( "h:i:s a" );
		
		$log_data = array (
				'sgb_board_id' => $sgb_board_id,
				'logged_by' => $id,
				'date' => $current_date,
				'time' => $current_time,
				
				'query' => $query 
		);
		$this->db->insert ( 'login_audit_log', $log_data );
	}

	
	
}